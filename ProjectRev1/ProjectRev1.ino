/*************************************************************
  Program BPM Sensor & DS18B20 Sensor
 *************************************************************/

#define BLYNK_PRINT Serial
#define BLYNK_TEMPLATE_ID "TMPLT4uKKSWO"
#define BLYNK_DEVICE_NAME "Project Health And Temperature"
#define BLYNK_AUTH_TOKEN "tUUAnQa1Dlt5hCNv0dOkIKxkD-ujqF5c"

#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

#define ONE_WIRE_BUS 13
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);  

float Temperature; 

char auth[] = BLYNK_AUTH_TOKEN;

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "YourNetworkName";
char pass[] = "YourPassword";

void setup()
{
  // Debug console
  sensors.begin();
  lcd.begin();
  lcd.backlight();
  Serial.begin(9600);
  
  Blynk.begin(auth, ssid, pass);
  
  lcd.setCursor(1,0);
  lcd.print("Project Health");
  lcd.setCursor(1,1);
  lcd.print("& Temperature!");
  delay(5000);
  lcd.clear();
}

void loop()
{
  // Send the command to get temperatures
  sensors.requestTemperatures(); 
  Temperature = sensors.getTempCByIndex(0);
  
  //print the temperature in Celsius
  Serial.print("Temperature: ");
  Serial.print(Temperature);
  Serial.println("°C");

  lcd.setCursor(1,1);
  lcd.print("Temp : ");
  lcd.print(Temperature);
  lcd.print((char)223);
  lcd.print("C");
  Blynk.virtualWrite(V1, Temperature);
  delay(100);
  
  Blynk.run();
}
