/*************************************************************
  Program BPM Sensor & DS18B20 Sensor
 *************************************************************/

#define BLYNK_PRINT Serial
#define BLYNK_TEMPLATE_ID "TMPLT4uKKSWO"
#define BLYNK_DEVICE_NAME "Project Health And Temperature"
#define BLYNK_AUTH_TOKEN "tUUAnQa1Dlt5hCNv0dOkIKxkD-ujqF5c"

#define REPORTING_PERIOD_MS     2000
uint32_t tsLastReport = 0;

#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include "MAX30105.h"
#include "spo2_algorithm.h"
 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 16, 2);

#define ONE_WIRE_BUS 13
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
MAX30105 particleSensor;  

#define MAX_BRIGHTNESS 255

// Setting Penyesuaian Pembacaan Sensor BPM.
uint32_t irBuffer[100];     //infrared LED sensor data
uint32_t redBuffer[100];    //red LED sensor data
int32_t bufferLength;       //data length
int32_t spo2;               //SPO2 value
int8_t validSPO2;           //indicator to show if the SPO2 calculation is valid
int32_t heartRate;          //heart rate value
int8_t validHeartRate;      //indicator to show if the heart rate calculation is valid
float Temperature; 

char auth[] = BLYNK_AUTH_TOKEN;

char ssid[] = "YourNetworkName";  // Your WiFi credentials.
char pass[] = "YourPassword";     // Set password to "" for open networks.

void setup()
{
  // Debug console
  sensors.begin();
  lcd.begin();
  lcd.backlight();
  Serial.begin(115200);
  
  Blynk.begin(auth, ssid, pass);

  // Initialize sensor
  if (!particleSensor.begin(Wire, I2C_SPEED_FAST)) //Use default I2C port, 400kHz speed
  {
    Serial.println(F("MAX30105 was not found. Please check wiring/power."));
    while (1);
  }

  Serial.println(F("Attach sensor to finger with rubber band. Press any key to start conversion"));
  Serial.read();

  byte ledBrightness = 100; //Options: 0=Off to 255=50mA
  byte sampleAverage = 4; //Options: 1, 2, 4, 8, 16, 32
  byte ledMode = 2; //Options: 1 = Red only, 2 = Red + IR, 3 = Red + IR + Green
  byte sampleRate = 200; //Options: 50, 100, 200, 400, 800, 1000, 1600, 3200
  int pulseWidth = 118; //Options: 69, 118, 215, 411
  int adcRange = 4096; //Options: 2048, 4096, 8192, 16384

  particleSensor.setup(ledBrightness, sampleAverage, ledMode, sampleRate, pulseWidth, adcRange);  //Configure sensor with these settings
  
  lcd.setCursor(1,0);
  lcd.print("Project Health");
  lcd.setCursor(1,1);
  lcd.print("& Temperature!");
  delay(5000);
  lcd.clear();
}

void loop()
{
bufferLength = 100; //buffer length of 100 stores 4 seconds of samples running at 25sps

  //read the first 100 samples, and determine the signal range
  for (byte i = 0 ; i < bufferLength ; i++)
  {
    while (particleSensor.available() == false) //do we have new data?
      particleSensor.check(); //Check the sensor for new data

    redBuffer[i] = particleSensor.getRed();
    irBuffer[i] = particleSensor.getIR();
    particleSensor.nextSample(); //We're finished with this sample so move to next sample

    Serial.print(F("red="));
    Serial.print(redBuffer[i], DEC);
    Serial.print(F(", ir="));
    Serial.println(irBuffer[i], DEC);
  }

  //calculate heart rate and SpO2 after first 100 samples (first 4 seconds of samples)
  maxim_heart_rate_and_oxygen_saturation(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate, &validHeartRate);

  //Continuously taking samples from MAX30102.  Heart rate and SpO2 are calculated every 1 second
  while (1)
  {
    //dumping the first 25 sets of samples in the memory and shift the last 75 sets of samples to the top
    for (byte i = 25; i < 100; i++)
    {
      redBuffer[i - 25] = redBuffer[i];
      irBuffer[i - 25] = irBuffer[i];
    }

    //take 25 sets of samples before calculating the heart rate.
    for (byte i = 75; i < 100; i++)
    {
      while (particleSensor.available() == false) //do we have new data?
        particleSensor.check(); //Check the sensor for new data

      redBuffer[i] = particleSensor.getRed();
      irBuffer[i] = particleSensor.getIR();
      particleSensor.nextSample(); //We're finished with this sample so move to next sample

     if (millis() - tsLastReport > REPORTING_PERIOD_MS) 
     {
        tsLastReport = millis();
        sensors.requestTemperatures(); 
     }
     
      if (heartRate < 0 || heartRate >= 160){
        heartRate = 0;
     }
    
       float Temperature = sensors.getTempCByIndex(0);

      //send samples and calculation result to terminal program through UART
      Serial.print(F("red="));
      Serial.print(redBuffer[i], DEC);
      Serial.print(F(", ir="));
      Serial.print(irBuffer[i], DEC);

      Serial.print(F(", HR="));
      Serial.print(heartRate, DEC);

      lcd.clear();
      lcd.setCursor(1,0);
      lcd.print("BPM  : ");
      lcd.print(heartRate);
  
      Serial.print(F(", HRvalid="));
      Serial.print(validHeartRate, DEC);

     /* Serial.print(F(", SPO2="));
      Serial.print(spo2, DEC);

      Serial.print(F(", SPO2Valid="));
      Serial.println(validSPO2, DEC); */
      
      Serial.print("Temperature: ");
      Serial.print(Temperature);
      Serial.println("°C");
      lcd.setCursor(1,1);
      lcd.print("Temp : ");
      lcd.print(Temperature);
      lcd.print((char)223);
      lcd.print("C");

      Blynk.virtualWrite(V0, heartRate);
      Blynk.virtualWrite(V1, Temperature);  
      Blynk.run();

   }
    //After gathering 25 new samples recalculate HR and SP02
    maxim_heart_rate_and_oxygen_saturation(irBuffer, bufferLength, redBuffer, &spo2, &validSPO2, &heartRate, &validHeartRate);
  }
}
